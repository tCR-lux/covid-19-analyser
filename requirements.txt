django

numpy
scipy
pandas
matplotlib

# gunicorn must be instantiated here for python3 to be ran from env
pytest
python-dateutil
python-dotenv
sphinx
sphinx-rtd-theme

# these one must not be from system
# replace the python3-gunicorn and gunicorn3
keyring
keyrings.alt
