#!/bin/bash
envchosen=$1

# Python Env
###################
# TODO: check if the virtual env exists

# TODO GET THE ENVNAME PARAMETER
# then, if ENVNAME == PROD
# -> --system-site-packages
# pip3 install -r requirements.txt
# some additional stuff https://stackoverflow.com/questions/55600132/installing-local-packages-with-python-virtualenv-system-site-packages
if [[ $envchosen = "ProductionConfig" ]];
then
  apt-get install python3-virtualenv
  xargs -a requirements-system.txt sudo apt-get install
  virtualenv --python=python3 --system-site-packages env 
else
  # it is faster for dev to rely on manual installation of virtualenv rather thant making it each time !
  virtualenv --python=python3 env
fi
# Here we are downloading and installing Flask without activating the virtual environment.
# However, given that we are using the pip from the environment itself, it achieves the same task.

env/bin/pip3 install -r requirements.txt
