https://docs.djangoproject.com/en/3.2/intro/tutorial02/

# Architecture

## in the root directory covid_platform:
##  - ./covid_platform is the Django project
##  - ./polls is the polling application

# Important notes on Django - Databases

## For data models, Django follows the DRY Principle. This includes the migrations - unlike in Ruby On Rails, for example, migrations are entirely derived from your models file, and are essentially a history that Django can roll through to update your database schema to match your current models.

## Django apps are “pluggable”: You can use an app in multiple projects, and you can distribute apps, because they don’t have to be tied to a given Django installation.

## Review the SQL commands a Django migrations would run
## python manage.py sqlmigrate polls 0001

## Check the Database status
## python manage.py check

## Primary keys (IDs) are added automatically. (You can override this, too.)
## By convention, Django appends "_id" to the foreign key field name. (Yes, you can override this, as well.)

## Some querry commands:
## Question.objects.all()
## q = Question.objects.filter(id=1)
## object = q.get()
## q.was_published_recently()
## Question.objects.filter(question_text__startswith='What')
## Question.objects.get(pub_date__year=current_year)
## Any primary key pk: Question.objects.get(pk=1)
## If Choices are related to Questions in a many-to-one RS, create child choice:
## object.choice_set.create(choice_text='Not much', votes=0)


## Timezones:
## https://docs.djangoproject.com/en/4.0/topics/i18n/timezones/

# Views
## Why do we use a helper function get_object_or_404() instead of automatically catching the ObjectDoesNotExist exceptions at a higher level, or having the model API raise Http404 instead of ObjectDoesNotExist?
## Because that would couple the model layer to the view layer. One of the foremost design goals of Django is to maintain loose coupling. Some controlled coupling is introduced in the django.shortcuts module.

## The template system uses dot-lookup syntax to access variable attributes. In the example of {{ question.question_text }}, first Django does a dictionary lookup on the object question. Failing that, it tries an attribute lookup – which works, in this case. If attribute lookup had failed, it would’ve tried a list-index lookup.