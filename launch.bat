cd db
cd inputs
IF exist COVID-19\ (
cd COVID-19
git pull
cd ..
) ELSE (
git clone https://github.com/CSSEGISandData/COVID-19.git
)
cd ..
cd ..
ECHO Activate Anaconda prompt and launch sequentially the python 3 script (needs Anaconda 3 distrbution (64 bits ?) !)
"%windir%\System32\cmd.exe "/K" %homedrive%%homepath%\AppData\Local\Continuum\anaconda3\Scripts\activate.bat %homedrive%%homepath%\AppData\Local\Continuum\anaconda3 && python ./src/cov-analyser.py"
pause